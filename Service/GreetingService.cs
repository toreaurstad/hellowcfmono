﻿using System;

namespace HelloWcf.Service
{
	public class GreetingService : IGreetingService
	{
		#region IGreetingService implementation
		public string Greet (string name)
		{
			return string.Format (@"Why hello {0}!", name); 
		}

		#endregion
		
	}
}


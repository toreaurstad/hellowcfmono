﻿using System;
using System.ServiceModel; 

namespace HelloWcf.Service 
{

	[ServiceContract]
	public interface IGreetingService
	{

		[OperationContract]
		[FaultContract(typeof(GreetingFault))]
		string Greet(string name);


	}
}


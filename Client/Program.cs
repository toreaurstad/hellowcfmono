﻿using System;
using System.ServiceModel;

namespace HelloWcf.Client
{
	class MainClass
	{
		static void GreetingServiceCall (GreetingServiceClient client)
		{
			Console.WriteLine ("\nEnter your name:");
			string name = Console.ReadLine ();
			string greeting = client.Greet (name);
			Console.WriteLine (greeting);

			Console.WriteLine(@"Another servicecall? [y/n]");
			ConsoleKeyInfo answer = Console.ReadKey();
			if (object.Equals(answer.KeyChar, 'y')) {
				GreetingServiceCall(client);
			}
		}

		public static void Main (string[] args)
		{
			var binding = new BasicHttpBinding ();
			EndpointAddress endpoint = new EndpointAddress ("http://localhost:8092");
			var client = new GreetingServiceClient (binding, endpoint);
			Console.WriteLine ("HelloWcf.Client demo:");
			GreetingServiceCall (client);

		}
	}
		
}

﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace HelloWcf.Client
{
	public class GreetingServiceClient : ClientBase<IGreetingService>, IGreetingService
	{
		#region IGreetingService implementation

		public string Greet (string name)
		{
			return Channel.Greet (name);
		}


		#endregion

		public GreetingServiceClient(Binding binding, EndpointAddress endpoint) : base(binding, endpoint)
		{
			
		}
	}
}


﻿using System;
using System.ServiceModel;
using HelloWcf.Service;
using System.ServiceModel.Description;
using System.IdentityModel;

namespace HelloWcf.Host
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			try {
				var serviceHost = new ServiceHost (typeof(GreetingService));

			     serviceHost.AddServiceEndpoint (typeof(IGreetingService),
					new BasicHttpBinding (), new Uri("http://localhost:8092"));

				serviceHost.Open ();
				Console.WriteLine (@"Press [CR] to end the WCF Service Host.");
						
				Console.ReadLine ();

				serviceHost.Close ();
			} catch (Exception err) {
				Console.WriteLine (@"An exception occured: {0}", err);
			}
		}
	}

}
